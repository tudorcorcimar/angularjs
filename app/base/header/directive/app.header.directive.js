(function() {
    /**
     * @desc application header directive
     * @example <app-header/>
     */
    angular
        .module('app')
        .directive('appHeader', appHeader);
    
    function appHeader() {
        return {
            restrict: 'E',
            transclude: true,
            templateUrl: '../app/base/header/views/header.html',
            scope: {},
            controller: ['$scope', function HeaderController($scope) 
            {
                $scope.headerTest = 'AngularJS Application';
            }]
        };
    }
})();