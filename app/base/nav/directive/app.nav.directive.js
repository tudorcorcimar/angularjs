(function() {
    /**
     * @desc application navigation directive
     * @example <app-nav/>
     */
    angular
        .module('app')
        .directive('appNav', appNav);
    
    function appNav() {
        return {
            restrict: 'E',
            transclude: true,
            templateUrl: '../app/base/nav/views/nav.html',
            scope: {},
            controller: ['$scope', function NavController($scope) 
            {
                var vm = this;
                
                function getMenu(){
                    return [
                        {
                            name: 'Home',
                            url: appConfig.baseUrl + ''
                        },
                        {
                            name: 'About Us',
                            url: appConfig.baseUrl + '/about-us'
                        },
                        {
                            name: 'Contacts',
                            url: appConfig.baseUrl + '/contacts'
                        }
                    ];
                };
                $scope.menu = getMenu();
            }]
        };
    }
    
})();