(function() {
    'use strict';
    angular
        .module('app')
        .service('ConsultantService', ConsultantService);

    function ConsultantService() 
    { 
        var vm = this;
        
        vm.findConsultant = function()
        {
            return 'Tomas Janks';
        };

    };
})();