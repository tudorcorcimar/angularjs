(function() {
    'use strict';
    angular.module('app').config(

        function config($routeProvider) {
            $routeProvider
                .when('/', {
                    templateUrl: '../app/home/views/home.html',
                    controller: 'HomeController',
                    controllerAs: 'vm'
                })
                .when('/about-us', {
                    templateUrl: '../app/home/views/aboutus.html',
                    controller: 'AboutUsController',
                    controllerAs: 'vm'
                })
                .when('/contacts', {
                    templateUrl: '../app/home/views/contacts.html',
                    controller: 'ContactsController',
                    controllerAs: 'vm'
                })
                .otherwise({ redirectTo: '/' });
        }

    );
})();
