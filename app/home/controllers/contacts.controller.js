(function() {
    'use strict';
    angular
        .module('app')
        .controller('ContactsController', ContactsController);

    function ContactsController($scope, ConsultantService) 
    { 
        var vm = this;

        vm.contactsView = 'contacts view';
        
        vm.bindMessage = function(event)
        {
            if(event.keyCode === 13){
                vm.messenger.correspondence.push(vm.messenger.message);
                vm.messenger.message = '';
            }
        };
        
        vm.messenger = {
            correspondence: [],
            message: '',
            consultant: {
                name: ConsultantService.findConsultant()
            },
            user: {
                name: 'visitor'
            }
        };
        
    };
})();